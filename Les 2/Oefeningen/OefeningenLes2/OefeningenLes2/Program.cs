﻿using System;

namespace OefeningenLes2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            //oefeninf met enviroment 
            bool is64bit = Environment.Is64BitOperatingSystem;
            string pcName = Environment.MachineName;
            int procCount = Environment.ProcessorCount;
            string userName = Environment.UserName;
            long memory = Environment.WorkingSet; //zal ongeveer 11 MB teruggeven.
            Console.WriteLine($"U computer heeft een 64bit systeem: {is64bit}");
            Console.WriteLine($"De naam van u pc is {pcName}");
            Console.WriteLine($"U heeft {procCount} processorkernen");
        }
    }
}
