﻿using System;

namespace Theorie_les_3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Random getallen");
            // wat is +-?
            //
            Random random = new Random();
            double poef = 0;

            Console.WriteLine("Voer bedrag in!");
            poef = random.Next(1, 100);
            Console.WriteLine($"De poef staat op {poef} euro");

            Console.WriteLine("Voer bedrag in!");
            poef += random.Next(1, 100);
            Console.WriteLine($"De poef staat op {poef} euro");

            Console.WriteLine("Voer bedrag in!");
            poef += random.Next(1, 100);
            Console.WriteLine($"De poef staat op {poef} euro");

            Console.WriteLine("Voer bedrag in!");
            poef += random.Next(1, 100);
            Console.WriteLine($"De poef staat op {poef} euro");

            Console.WriteLine("Voer bedrag in!");
            poef += random.Next(1, 100);
            Console.WriteLine($"De poef staat op {poef} euro");

            Console.WriteLine("**");
            Console.WriteLine($"Het totaal bedrag van de poef is {poef} euro");
            Console.WriteLine($"Dit zal {Math.Ceiling(poef / 10)} afbetalingen vragen");
        }
    }
}
