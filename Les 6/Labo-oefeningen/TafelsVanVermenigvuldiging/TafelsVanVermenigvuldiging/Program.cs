﻿using System;

namespace TafelsVanVermenigvuldiging
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Tafels van vermenigvuldiging");
            //buitenste for maakt rijen
            Console.WriteLine('\t');
            for (int i = 1; i < 10; i++)
            {
                //binnenste maakt kolommen
                for (int j =1; j < 10; j++)
                {
                    if (j == 1)
                    {
                        Console.Write($"{ i}\t");
                    }
                    //als i = 1, is dat de tafel van 1
                    Console.Write($"{j * i}\t");
                }
                Console.WriteLine();
            }
        }
    }
}
