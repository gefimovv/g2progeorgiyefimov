﻿using System;

namespace ForDoordenker
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Console.WriteLine("typ een getal in: ");
            byte breedte = Convert.ToByte(Console.ReadLine());
            //aantal regels printen
            for (byte i = 1; i <= breedte; i++)
            {

                //alsbde eerste lijn, 1 ster / als tweede lijn, 2 ster
                for (byte j = 1; j <= i; j++)
                {
                    Console.Write('*');
                }
                Console.WriteLine(); 
            }
            for (byte i = --breedte; i >= 1; i--)
            {
                for (byte j = 1; j <= i; j++)
                {
                    Console.Write('*');
                }
                Console.WriteLine();
            }
            Console.ReadKey();
        }
    }
}
