﻿using System;

namespace Aftellen
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Aftellen");
            //We beginnen pet 10 en we tellen af tot 0 en dan zeggen we beep
            for (int i = 10; i > -2; i--)
            {
                Console.Beep();
                Console.WriteLine(i);
            }
            Console.Beep();
        }
    }
}
