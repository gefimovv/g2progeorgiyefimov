﻿using System;

namespace TestLussen
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int a = 0; a <= 10; a++)
            {
                Console.Write(a + "\t");
                for (int b = 1; b <= 10; b++)
                {
                    if (a > 0) Console.Write(a * b + "\t");
                    else Console.Write(b + "\t");
                }
                Console.Write("\n");

            }

            Console.ReadLine();
        }
    }
}
