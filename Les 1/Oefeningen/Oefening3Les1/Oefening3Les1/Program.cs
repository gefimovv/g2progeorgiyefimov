﻿using System;

namespace Oefening3Les1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Geef het aantal liter aan in tank voor de rit:");
            string litervoorrit = Console.ReadLine();
            Console.WriteLine("geef het aantal liter na de rit:");
            string liternarit = Console.ReadLine();
            Console.WriteLine("geef aantal kilometerstand voor de rit:");
            string kmvoorrit = Console.ReadLine();
            Console.WriteLine("Geef aantal km na de rit");
            string kmnarit = Console.ReadLine();
            //cosoleclear zorgt ervoor dat nadat jij op de vragen hebt beantwoord jij de vragen niet meer te zien krijgt//
            Console.Clear();
            Console.WriteLine($"Liter voor de rit {litervoorrit}");
            Console.WriteLine($"Liter na de rit {liternarit}");
            Console.WriteLine($"Km voor de rit {kmvoorrit}");
            Console.WriteLine($"Km na de rit {kmnarit}");

            decimal litervoorrit2 = Convert.ToDecimal(litervoorrit);
            decimal liternarit2 = Convert.ToDecimal(liternarit);
            decimal kmvoorrit2 = Convert.ToDecimal(kmvoorrit);
            decimal kmnarit2 = Convert.ToDecimal(kmnarit);

            decimal verbuik = (100 * (litervoorrit2 - liternarit2) / (kmnarit2 - kmvoorrit2));

            Console.WriteLine($"het verbuik van de auto is: {verbuik}");
            Console.WriteLine($"afgeronde verbruik is: {verbuik,0:F2}");

        }
    }
}
