﻿using System;

namespace OefeningRekenmachine
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Oefening rekenmachine");
            Console.WriteLine(-1 + 4 * 6);
            Console.WriteLine((35 + 5) % 7);
            Console.WriteLine(14 + -4 * 6 / 11);
            Console.WriteLine(2 + 15 / 6 * 1 - 7 % 2);

            // gemiddelde van 18 11 en 8

            float getal1 = 18;
            float getal2 = 12;
            float getal3 = 8;
            Console.WriteLine((getal1 + getal2 + getal3) / 3);

            //vermenigvuldiging van 411 elke keer op enter is blijven vermenigvuldigen

            int getal5 = 1;
            int getal4 = 1;
            int result = 411;
            Console.WriteLine((getal5++) + "* 411 is" + (getal4++ * result));
            Console.ReadLine();
            Console.Clear();
            Console.WriteLine((getal5++) + "* 411 is" + (getal4++ * result));
            Console.ReadLine();
            Console.Clear();
            Console.WriteLine((getal5++) + "* 411 is" + (getal4++ * result));
            Console.ReadLine();
            Console.Clear();
            //als ik nu Console.WriteLine((getal5++) + "* 411 is" + (getal4++ * result)); blijf copy pasten blijfik extra lagen enter bijvullen

            //oefeno,g zwaartekracht
            double mijngewicht = 100;
            double mercury = 0.38;
            double venus = 0.91;
            double aarde = 1;
            Console.WriteLine($"mijn gewocht op mercury  is {(mijngewicht * mercury)}");
        
        }
    }
}
