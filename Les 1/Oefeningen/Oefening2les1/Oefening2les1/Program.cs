﻿using System;

namespace Oefening2les1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("geef het eerste getal:");
            int getal1;
            getal1 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("geef het tweede getal:");
            int getal2;
            getal2 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine ($"de som van deze getal is: {getal1 + getal2}");

            //wat je ook kan doen is boven de laatste cosole writeline dit schrijven//
            //int som = getal1 + getal2//
            //en dan zo consolee.writeline($"tekst! {som}"); //


        }
    }
}
