﻿using System;

namespace OefeningLes1Advanced2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("Wat is je favoriete kleur");
            Console.ResetColor();
            string kleur;
            kleur = Console.ReadLine();
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine("Wat is je favoriete eten?");
            Console.ResetColor();
            string eten;
            eten = Console.ReadLine();
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Wat is je favoriete film?");
            Console.ResetColor();
            string film;
            film = Console.ReadLine();

            Console.WriteLine("Jou fovoriete kleur is " + kleur + " en je eet graag " + eten + " en je favoriete film is " + film);
        }
    }
}
